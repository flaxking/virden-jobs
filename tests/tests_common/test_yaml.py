import unittest
from unittest.mock import patch, mock_open

import virden_jobs.common.yaml


class YAMLTestCase(unittest.TestCase):
    def test_open_should_return_dict(self):

        mock_open_content = mock_open(read_data='data: content')

        with patch('builtins.open', mock_open_content) as mock_file:
            dictionary = {'data': 'content'}
            value = virden_jobs.common.yaml.load('path/to/file')
            mock_file.assert_called_with('path/to/file', 'r')
            self.assertDictEqual(dictionary, value)


if __name__ == '__main__':
    unittest.main()
